package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Rook implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        // TODO: https://en.wikipedia.org/wiki/Rook_(chess)
        // TODO: Prosze dokonczyc implementacje...

        if(moveFrom.getX() == moveTo.getX() && moveFrom.getY() == moveTo.getY()) {
            return false;
        }

        return moveFrom.getX() == moveTo.getX() || moveFrom.getY() == moveTo.getY();
    }
}
