package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;

public class Pawn implements RulesOfGame {

    @Override
    public boolean isCorrectMove(Point2d moveFrom, Point2d moveTo) {

        // TODO: https://en.wikipedia.org/wiki/Pawn_(chess)
        // TODO: Prosze dokonczyc implementacje...

        return moveFrom.getX() == moveTo.getX() && moveFrom.getY() + 1 == moveTo.getY() ||
                moveFrom.getX() == moveTo.getX() && moveFrom.getY() + 2 == moveTo.getY() ||
                moveFrom.getX() == moveTo.getX() && moveFrom.getY() - 2 == moveTo.getY() ||
                moveFrom.getX() == moveTo.getX() && moveFrom.getY() - 1 == moveTo.getY();

    }
}
