package com.demo;

import com.demo.main.Point2d;
import com.demo.main.RulesOfGame;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class KnightTest {

    private RulesOfGame knight;
    private Point2d pointFrom;
    private Point2d pointTo;

    @BeforeEach
    public void initDataForKnight() {

        knight = new Knight();
        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(0, 0);
    }

    @Test
    public void checkCorrectMoveForKnight() {

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(10, 10);
        assertFalse(knight.isCorrectMove(pointFrom, pointTo), "Incorrect move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(10, 10);
        pointTo = new Point2d(8, 9);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(2, 1);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(0, 0);
        pointTo = new Point2d(1, 2);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(4, 3);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(3, 4);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(6, 3);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);

        pointFrom = new Point2d(5, 5);
        pointTo = new Point2d(7, 4);
        assertTrue(knight.isCorrectMove(pointFrom, pointTo), "Correct move from" + pointFrom + "to" + pointTo);
    }
}
